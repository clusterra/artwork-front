# Artwork.pro project #

[![Apache License](https://img.shields.io/badge/Licence-Apache%202.0-blue.svg?style=flat-square)](http://www.apache.org/licenses/LICENSE-2.0)    



The 'social' project to give designers what they have needed all the time:
A collaborative service to share design ideas.



# Front-End part (browser-based client) #

### NB. No production compression/optimization mechanism applied so far. The files are in a flat filetree structure that can be served by a static proxy server (e.g. `nginx`) as is ###

## How to Set Up Dev env under Windows##
1. Install nginx (preferably at the same level as `artwork-front` repo) to handle paths easily
2. Copy nginx config from this repo (`nginx/windows/nginx.conf`, `nginx/windows/nginx/conf.d/*`) to nginx installation folder
3. Edit both copied files in place
	- in `nginx.conf' make sure upstream server is correct (typically 127.0.0.1:8080)
 	- in `conf.d/stage.conf` set `root` to `..\..\artwork-front\public;`

4. To be able to test sign in from the social networks, edit etc.hosts, add the follwoing line:
`127.0.0.1  stage.artwork.pro`

