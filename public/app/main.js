requirejs.config({
    paths: {
        'app':  '/app',
        'text': '/lib/require/text',
        'bindings': '/lib/bindings',
        '3rds': '/lib/3rds',
        'durandal':'/lib/durandal/js',
        'plugins' : '/lib/durandal/js/plugins',
        'knockout': '/lib/knockout/knockout-3.1.0',
        'validation': '/lib/knockout/knockout.validation-2.0.3.min',
        'punches': '/lib/knockout/knockout.punches.min',
        'bootstrap': 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min',
        'jquery': 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min'
    },
    shim: {
        'bootstrap': {
            deps: ['jquery'],
            exports: 'jQuery'
        }
    }
});


define(['durandal/system', 'durandal/app', 'durandal/viewLocator', 'bootstrap', 'jquery', 'knockout', '3rds/moment.min', '3rds/moment.ru', 'validation', 'punches', '3rds/timeago'],
    function (system, app, viewLocator, bs, $, ko, moment) {

    //>>excludeStart("build", true);
    system.debug(true);
    //>>excludeEnd("build");

    app.title = 'Artwork';

    //specify which plugins to install and their configuration
    app.configurePlugins({
        router:true,
        dialog: true,
        bootstrapModal: true
        //widget: {
            //kinds: ['expander']
        //}
    });

    ko.validation.init({insertMessages: false, decorateInputElement: true, errorElementClass: 'error'});
    ko.punches.enableAll();
        
    moment.locale('ru');

    ko.filters.shortDate = function(date) {
        return moment(date).format('D MMMM В HH:MM').toUpperCase();
    };

    ko.filters.timeAgo = function(date) {
        return $.timeago(new Date(date));
    };

   /* ko.filters.paragraphs = function(text) {
        return text.replace(/\n/g, '<br/>');
    };*/

    app.start().then(function () {
        viewLocator.useConvention();
        app.setRoot('shell/shell', null, 'content');
    });

});