define(['durandal/system', 'plugins/http'], function(system, http) {

    var pageSize = 16;

    return {
        /**
         * orderBy: 'createdDate' | 'likeable.likesCount' | 'viewable.viewsCount'
         */
        getWorks: function(orderBy) {
            return system.defer(function(defer) {
                http.get('/api/works?page=0&size=' + pageSize + '&sort=' + orderBy + ',desc').done(function (works) {
                    defer.resolve(works._embedded ? works._embedded.workResourceList : []);
                }).fail(function () {
                    // TODO
                    defer.reject();
                });
            }).promise();
        },
        
        // workaround for infinite scroll
        getWorksServiceUrl: function(pageIdx, orderBy) {
            return '/api/works?page=' + pageIdx + '&size=' + pageSize + '&sort=' + orderBy + ',desc';        
        },

        // workaround for infinite scroll
        getWorksFromRawResponse: function(response) {
            return response._embedded ? response._embedded.workResourceList : [];
        },
        
        getWork: function(workId) {
            return system.defer(function(defer) {
                http.get('/api/works/' + workId).done(function(work) {
                    defer.resolve(work);
                }).fail(function () {
                    // TODO
                    defer.reject();
                });
            }).promise();
        },


        getWorkComments: function(workId) {
            return system.defer(function(defer) {
                http.get( '/api/comments/?commentableId=' + workId).done(function(comments) {
                    defer.resolve(comments._embedded ? comments._embedded.commentResourceList : []);
                }).fail(function () {
                    // TODO
                    defer.reject();
                });
            });
        },


        addComment: function(workId, comment) {
            return system.defer(function(defer) {
                http.post( '/api/comments/?commentableId=' + workId, comment).done(function(comments) {
                    defer.resolve();
                }).fail(function () {
                    // TODO
                    defer.reject();
                });
            });
        },


        deleteWork: function(workId) {
             return system.defer(function(defer) {
                 http.remove('/api/works/' + workId).done(function () {
                    defer.resolve();
                 }).fail(function () {
                     // TODO
                     defer.reject();
                 });
              }).promise();
        },

        updateWorkDescription: function(workId, newDescription) {
             return system.defer(function(defer) {
                  http.put('/api/works/' + workId, {"description": newDescription}).done(function () {
                       defer.resolve();
                  }).fail(function () {
                       // TODO
                       defer.reject();
                  });
                }).promise();
        },

        likeWork: function(workId) {
            http.put('/api/works/' + workId + '?like=true', null, function(data){
            });
        },

        unlikeWork: function(workId) {
            http.put('/api/works/' + workId + '?like=false', null, function(data){
            });
        },

        likeComment: function(workId, commentId) {
             http.put('/api/works/' + workId + '/comments/' + commentId + '?like=true', null, function(data){
            });
        },

        unlikeComment: function(workId, commentId) {
            http.put('/api/works/' + workId + '/comments/' + commentId + '?like=false', null, function(data){
            });
        }
    }

});