define(['durandal/system', 'plugins/http'], function(system, http) {

    var PROFILE = {}; // this var will be anyway thread safe, so this service is stateful

    return {
        syncProfile: function() {
            return system.defer(function(defer) {
                http.get('/login').done(function (profile) {
                        profile.role = profile.authorities;
                        profile.avatarUrl = profile.imageUrl;
                        profile.isLoggedIn = (profile.authorities != 'ROLE_ANONYMOUS');
                        profile.principalId = profile.principal;
                        profile.provider = profile.providerId;

                        PROFILE = profile;

                        defer.resolve(profile);
                    }).fail(function (jqXHR) {
                        if (jqXHR.status == 401) {
                            PROFILE = {};
                            PROFILE.isLoggedIn = false;
                            defer.resolve(PROFILE);
                        }  else {
                            defer.reject();
                        }
                    });
            }).promise();
        },

        profile: function() {
            return PROFILE;
        },

        signOut: function() {
            return system.defer(function(defer) {
                http.remove('/login').always (
                    function () {
                        defer.resolve();
                    }
                )
            }).promise();
        }
    }

});