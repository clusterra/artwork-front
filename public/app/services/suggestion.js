define(['durandal/system', 'plugins/http'], function(system, http) {

    return {
        startNewSuggestionThread: function(workId, x, y, suggestion) {
            return system.defer(function(defer) {
                http.post('/api/works/' + workId + '/suggestions',
                    {"text": suggestion, "pointX": Math.round(x), "pointY": Math.round(y)})
                    .done(function () {
                    defer.resolve();
                }).fail(function () {
                    // TODO
                    defer.reject();
                });
            }).promise();
        },


        getWorkSuggestions: function(workId) {
            return system.defer(function(defer) {
                http.get( '/api/works/' + workId + '/suggestions').done(function(suggestions) {
                    defer.resolve(suggestions._embedded ? suggestions._embedded.suggestionResourceList : []);
                }).fail(function () {
                    // TODO
                    defer.reject();
                });
            });
        }
    }
});

