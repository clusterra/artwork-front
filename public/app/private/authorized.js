define(['durandal/app', 'services/security', 'plugins/dialog', 'private/upload', 'plugins/router', '3rds/notifyfx', 'bindings/popover'],
    function(app, securityService, dialog, UploadVM, router) {

    function reloadApp() {
        window.location.href = '/';
    }


    app.on('work.added').then(function(dialogModelCtor){
        dialog.close(dialogModelCtor);
        
        var notification = new NotificationFx({
            message : '<i class="notify notify-megaphone"></i><p>Поздравляем! Ваша работа успешно загружена и доступна в списке работ.</p>',
            layout : 'bar',
            effect : 'slidetop',
            type : 'notice', // notice, warning or error
            onClose : function() {}
        });

        notification.show();

        if (router.activeInstruction().fragment === '') {
            app.trigger('workmart.refresh');
        } else {
            router.navigate('/');
        }

    });

    return {
        uploadWork: function() {
            dialog.showBootstrapDialog(UploadVM);
        },

        
        signOut: function() {
            securityService.signOut().then(function() {
                reloadApp();
            });
        }
    }
});