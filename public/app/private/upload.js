define(['durandal/app', 'knockout',  'bindings/dropzone'], function(app, ko) {

    var ctor = function() {
        var dropzoneInstance,
            fileAdded = ko.observable(false),
            wasServerError = ko.observable(false);

        var uploading = ko.observable(false);
        var description = ko.observable().extend({
            rateLimit: { timeout: 500, method: 'notifyWhenChangesStop' },
            required: {
                params: true,
                message: 'Пожалуйста заполните это поле'
            },
            minLength: {
                params: 50,
                message: 'Введите по меньшей мере {0} символов'
            },
            maxLength: {
                params: 400,
                message: 'Текст не может превышать {0} символов'
            }
        });

        var formValid = ko.computed(function() {
            return description.isValid() && fileAdded() && !wasServerError();
        });



        var dropzoneConfig = {
            maxFilesize: 5, //Mb
            uploadMultiple: false,
            maxFiles: 1,
            acceptedFiles: 'image/*',
            autoProcessQueue: false,
            clickable: true,
            dictDefaultMessage: '<i class="icon chapter_iconsupload"></i><br>ПЕРЕТАЩИТЕ ФАЙЛ СЮДА<br><span class="or">ИЛИ</span><br><a href="javascript:;" class="choose_file">ВЫБЕРИТЕ ИЗ КАТАЛОГА</a>',
            dictInvalidFileType: 'Вы не можете загрузить файл этого типа',
            dictFileTooBig: 'Файл слишком большой, максимальный размер {{maxFilesize}} Мб',

            instanceCreated: function(instance) {
                dropzoneInstance = instance;
            },

            fileAdded: function(file) {
                fileAdded(true);
                wasServerError(false);
            },

            maxFilesExceeded: function(file) {
                wasServerError(false);
            },

            success: function(file) {
                app.trigger('work.added', ctor);
            },

            error: function(file, response) {
                wasServerError(true);
                // TODO change some view model props
                //var errorMessage = response[0].defaultMessage;
                //$(file.previewElement).find('.dz-error-message').text(errorMessage);
                //$submitBtn.removeClass('active');
                //disableSubmit();
            },

            uploadProgress: function(file, progress, bytesSent) {
                // TODO show the real progress, not imitated
            }

        };



        return {
            dropzoneConfig: dropzoneConfig,
            description: description,
            formValid: formValid,
            uploadFile: function() {
                uploading(true);
                dropzoneInstance.processQueue();
            },
            uploading: uploading
        }
    };
    
    return ctor;

});