define(['knockout'], function(ko) {

    var apiPrefix = '/login';

    var oauthProviders = [
        {apiPath: apiPrefix + '/vkontakte', scope: 'offline,email', label: 'Вконтакте', btnClass: 'btn-vk', faClass: 'fa-vk', disabled: ko.observable(false)},
        {apiPath: apiPrefix + '/facebook', scope: 'public_profile,email', label: 'Facebook', btnClass: 'btn-facebook', faClass: 'fa-facebook', disabled: ko.observable(false)}
    ];


    return function() {

        var disableAllExceptClicked = function(clickedProvider) {
            oauthProviders.forEach(function(provider) {
                if (clickedProvider.apiPath !== provider.apiPath) {
                    provider.disabled(true);
                }
            });
        };


        return {
            oauthProviders: oauthProviders,

            submit: function(oauthProvider) {
                disableAllExceptClicked(oauthProvider);
                return true;
            }
        }
    }
   
});