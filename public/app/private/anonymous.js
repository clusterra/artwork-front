define(['plugins/dialog', 'private/signin'], function(dialog, SigninVM) {

    return {
        showSignIn: function() {
            dialog.showBootstrapDialog(SigninVM);
        }
    }
});