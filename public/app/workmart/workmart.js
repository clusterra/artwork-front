define(['durandal/app', 'knockout', 'services/work', 'services/security', 'bindings/animolike', 'bindings/animoprogress', 'bindings/infiscroll'], 
    function(app, ko, workService, securityService) {

    var works = ko.observableArray();
    var isReady = ko.observable(false);
    
    var sortedBy = ko.observable('createdDate');

    var isAnimationFired = ko.observable(false);

    var lastPageLoaded = 1;
    var nextPageServiceUrl = ko.observable(workService.getWorksServiceUrl(lastPageLoaded, sortedBy()));

    var resetInfiScrollFn, destroyInfiScrollFn;

    var anyWorks = ko.observable(false); // not ko.computed, because need to manual change it


    app.on('workmart.refresh').then(function(){
         resetInfiniteScrollAndLoadInitialWorks();
    });


    var ableToUpload = securityService.profile().isLoggedIn;

    function resetInfiniteScrollAndLoadInitialWorks() {
        works.removeAll();

        lastPageLoaded = 1;
        nextPageServiceUrl(workService.getWorksServiceUrl(lastPageLoaded, sortedBy()));

        if (resetInfiScrollFn) {
            resetInfiScrollFn();
        }

        fetchWorksInitial(sortedBy());
    }


    function topUpWorks(moreWorks) {
        for (var i = 0; i < moreWorks.length; i++) {
            works.push(moreWorks[i]);
        }

        anyWorks(moreWorks.length > 0);
    }

    // called only on page load/manual refresh
    function fetchWorksInitial(sortTerm) {
        isReady(false);

        workService.getWorks(sortTerm).then(function(w) {
            topUpWorks(w);



            isReady(true);
            isAnimationFired(true);
        });
    }


    function onLoadAdditionalWorks(response) {
        var w = workService.getWorksFromRawResponse(response);

        topUpWorks(w);

        nextPageServiceUrl(workService.getWorksServiceUrl(++lastPageLoaded, sortedBy()));
    }
  

    return {
        activate: function() {
            works.removeAll();
            lastPageLoaded = 1;
            nextPageServiceUrl(workService.getWorksServiceUrl(lastPageLoaded, sortedBy()));

            fetchWorksInitial(sortedBy());
        },

        deactivate: function() {
            // destroy infiscroll
            destroyInfiScrollFn();
        },

        works: works,

        isReady: isReady,
        
        sortedBy: sortedBy,

        sortBy: function(sortTerm) {
            sortedBy(sortTerm);
            resetInfiniteScrollAndLoadInitialWorks();
        },

        isAnimationFired: isAnimationFired,

        nextPageServiceUrl: nextPageServiceUrl,

        onLoadAdditionalWorks: onLoadAdditionalWorks,

        resetInfiscroll: function(resetFn) {
            resetInfiScrollFn = resetFn;
        },

        destroyInfiscroll: function(destroyFn) {
            destroyInfiScrollFn = destroyFn;
        },

        likeWork: function(workId) {
            workService.likeWork(workId);
        },
        
        unlikeWork: function(workId) {
            workService.unlikeWork(workId);
        },

        ableToUpload: ableToUpload,

        anyWorks: anyWorks

    }


});