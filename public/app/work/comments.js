define(['durandal/app', 'durandal/system', 'knockout', 'services/work', 'services/security', 'bindings/animolike'], function(app, system, ko, workService, securityService) {

    var comments = ko.observableArray();
    var ableToComment = securityService.profile().isLoggedIn;

    var workId;


    function fetchComments(wId, defer) {
        workId = wId;
        comments.removeAll();

        workService.getWorkComments(workId).then(function(c) {
            for (var i = 0; i < c.length; i++) {
                 comments.push(c[i]);
            }

            if (defer) {
                defer.resolve();
            }
         });
    }

    app.on('comments.refresh').then(function(workId){
        fetchComments(workId);
    });


    return {

        activate: function(workId) {
            return system.defer(function(defer) {
                fetchComments(workId, defer);
            });
        },



        comments: comments,

        ableToComment: ableToComment,

        workId: function () {
            return workId;
        },

        likeComment: function(ids) {
            workService.likeComment(ids.workId, ids.commentId);
        },

        unlikeComment: function(ids) {
            workService.unlikeComment(ids.workId, ids.commentId);
        }
    }

});