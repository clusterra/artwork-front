define(['plugins/dialog'], function(dialog) {

     var ctor =  function() {

        return {
            confirmDelete: function() {
                dialog.close(ctor, 'Delete');
            }
        }
     };

     return ctor;
});