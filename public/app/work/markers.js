define(['durandal/app', 'knockout', 'services/suggestion', 'bindings/planit'],
    function(app, ko, suggestionService) {

    var parentCtx;
    var suggestions = ko.observableArray();
        
        
    var ctor = function() {
        

        function startSuggestionThread(x, y, text) {
            suggestionService.startNewSuggestionThread(parentCtx.workId(), x, y, text).then(function() {
                alert('Suggestion added!')
            });
        }




        return {
            parentCtx: parentCtx,
            suggestions: suggestions,
            startSuggestionThread: startSuggestionThread
        }
    };

    ctor.activate = function(data) {
        parentCtx = data;

        suggestions.removeAll();
        
        suggestionService.getWorkSuggestions(parentCtx.workId()).then(function (s) {            
            for (var i = 0; i < s.length; i++) {
                suggestions.push(s[i]);
            }
        });
    };

    return ctor;
});