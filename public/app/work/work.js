define(['durandal/app', 'knockout', 'services/work', 'services/security', 'plugins/router', 'plugins/dialog', 'work/confirm', 'work/markers',    
    'bindings/animolike', 'bindings/inplaceedit', 'bindings/photoswipe', '3rds/notifyfx'],
function(app, ko, workService, securityService, router, dialog, ConfirmVM, MarkersVM) {

    return function() {
        var workId = ko.observable();
        var viewsCount = ko.observable();
        var likesCount = ko.observable();
        var commentsCount = ko.observable();
        var createdDate = ko.observable();
        var originalUri = ko.observable();
        var ownerDisplayName = ko.observable();
        var ownerUrl = ko.observable();
        var description = ko.observable();
        var canLike = ko.observable();
        var canUnlike = ko.observable();
        var isMine = ko.observable();

        var descriptionEditIsInProgress = ko.observable(false);
        var enteringComment = ko.observable(false);

        var activateInPlaceEditFn, deactivateInPlaceEditFn;

        var ableToComment = securityService.profile().isLoggedIn;
        var newComment = ko.observable('');

        var commentFormValid = ko.computed(function() {
            return newComment().length > 0 && newComment().length < 5000;
        });


        function hideCommentForm() {
            setTimeout(function() {
                enteringComment(false);
            }, 100);
        };

        var ctx = this;

        this.activate = function() {
            workId(router.activeInstruction().params[0]);

            workService.getWork(workId()).then(function(work) {
                viewsCount(work.viewsCount);
                likesCount(work.likesCount);
                commentsCount(work.commentsCount);
                createdDate(work.createdDate);
                originalUri(work.imageResource.originalUri);
                ownerDisplayName(work.owner.displayName);
                ownerUrl(work.owner.imageUrl);
                description(work.description);
                isMine(work.isMine);
                canLike(work._links.like != undefined);
                canUnlike(work._links.unlike != undefined);
            });

        };

        this.workId = workId;
        this.viewsCount = viewsCount;
        this.likesCount = likesCount;

        this.commentsCount = commentsCount;
        this.createdDate = createdDate;
        this.originalUri = originalUri;
        this.ownerDisplayName = ownerDisplayName;
        this.ownerUrl = ownerUrl;
        this.description = description;
        this.isMine = isMine;
        this.canLike = canLike;
        this.canUnlike = canUnlike;

        this.descriptionEditIsInProgress = descriptionEditIsInProgress;
        this.enteringComment = enteringComment;

        this.likeWork = function(workId) {
             workService.likeWork(workId);
        };

        this.unlikeWork = function(workId) {
             workService.unlikeWork(workId);
        };

        this.deleteWork = function() {
            dialog.showBootstrapDialog(ConfirmVM).then(function(result) {
                if (result == 'Delete') {
                    workService.deleteWork(workId()).then(function() {
                         var notification = new NotificationFx({
                              message : '<i class="notify notify-megaphone"></i><p>Ваша работа была удалена.</p>',
                              layout : 'bar',
                              effect : 'slidetop',
                              type : 'warning', // notice, warning or error
                              onClose : function() {}
                         });

                         notification.show();

                         router.navigate('/');
                    });
                }
            });
        };


        this.editDescription = function() {
            activateInPlaceEditFn();
            descriptionEditIsInProgress(true);
        };

        this.descriptionEdited = function(newDescription) {
            description(newDescription);

            workService.updateWorkDescription(workId(), newDescription).then(function() {
                 descriptionEditIsInProgress(false);

                 var notification = new NotificationFx({
                      message : '<i class="notify notify-megaphone"></i><p>Описание работы было успешно отредактировано.</p>',
                      layout : 'bar',
                      effect : 'slidetop',
                      type : 'notice', // notice, warning or error
                      onClose : function() {}
                      });

                 notification.show();
            });
        };


        this.leaveComment = function() {
            setTimeout(function() {
               enteringComment(true);
            }, 100);
        };

        this.hideCommentForm = hideCommentForm;


        this.markOnWork = function() {
            dialog.showMarkersDialog(MarkersVM, ctx);
        };

        this.activateInPlaceEdit = function(activateFn) {
            activateInPlaceEditFn = activateFn;
        };

        this.onCancelEdit = function() {
            descriptionEditIsInProgress(false);
        };


        this.ableToComment = ableToComment;
        this.commentFormValid = commentFormValid;
        this.newComment = newComment;

        this.sendComment = function(data, e) {
            hideCommentForm();

            workService.addComment(workId(), newComment()).then(function() {
                newComment(''); // clear comment form

                app.trigger('comments.refresh', workId());
            });
        }


        this.validateDescription = function(value) {
            var trimmed = value.trim();

            if (trimmed.length == 0) {
                return 'ПОЖАЛУЙСТА ЗАПОЛНИТЕ ЭТО ПОЛЕ';
            }

            if (trimmed.length < 50) {
                return 'ВВЕДИТЕ ПО МЕНЬШЕЙ МЕРЕ 50 СИМВОЛОВ';
            }

            if (trimmed.length > 400) {
                return 'ТЕКСТ НЕ МОЖЕТ ПРЕВЫШАТЬ 400 СИМВОЛОВ';
            }
        };

    }
});