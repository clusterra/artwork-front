define(['plugins/router', 'services/security', 'knockout', 'private/anonymous', 'private/authorized'],
function (router, securityService, ko, anonVM, authorizedVM) {  

    return {
        router: router,

        activate: function () {
            var self = this;
           
            return securityService.syncProfile().then(function(p) {
                self.profile = p;
                self.privateSectionVM(p.isLoggedIn ? authorizedVM : anonVM);
            }).then(function() {
                router.map([
                    { route: '', moduleId: 'workmart/workmart', title: '',  nav: 1 },
                    { route: 'works/:id', moduleId: 'app/work/work', title: 'Работа', nav: false }
                ]).buildNavigationModel()
                     .mapUnknownRoutes('workmart/workmart', '')
                     .activate({pushState: true});
            });
        },

        profile: null,

        privateSectionVM: ko.observable()
    };
});