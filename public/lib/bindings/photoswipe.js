define(['knockout', 'jquery', '3rds/photoswipe.min', '3rds/photoswipe-ui-default.min'], function(ko, $, PhotoSwipe, PhotoSwipeUI_Default) {

    ko.bindingHandlers.photoswipe = {
        init: function (element) {
            $(element).click(function() {
                var pswpElement = document.querySelectorAll('.pswp')[0];

                // build items array
                var items = [
                    {
                        src: $(element).prop('src'),
                        w: $(element).prop('naturalWidth'),
                        h: $(element).prop('naturalHeight')
                    }
                ];

                // define options (if needed)
                var options = {
                    // history & focus options are disabled on CodePen
                    history: false,
                    focus: false,

                    showAnimationDuration: 0,
                    hideAnimationDuration: 0
                };

                var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();
            });
        }
    };

});