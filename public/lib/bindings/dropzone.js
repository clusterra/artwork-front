define(['knockout', 'jquery', '3rds/dropzone'], function(ko, $) {

    ko.bindingHandlers.dropzone = {
        init: function (element, valueAccessor) {
            var options = ko.utils.unwrapObservable(valueAccessor());
            var config = options.config;

            $(element).dropzone({
                maxFilesize: config.maxFileSize || 5, //Mb
                uploadMultiple: config.uploadMultiple,
                maxFiles: config.maxFiles || 1,
                previewTemplate: $(options.template).html(),
                acceptedFiles: config.acceptedFiles || 'image/*',
                autoProcessQueue: config.autoProcessQueue,
                clickable: config.clickable,
                dictDefaultMessage: config.dictDefaultMessage,
                dictInvalidFileType: config.dictInvalidFileType,
                dictFileTooBig: config.dictFileTooBig,
                init: function() {
                     this.on('addedfile', function(file) {
                         config.instanceCreated(this);
                         config.fileAdded(file);
                     });

                     this.on('maxfilesexceeded', function(file) {
                          this.removeAllFiles();
                          this.addFile(file);
                          config.fileAdded(file);
                          config.maxFilesExceeded(file);
                     });

                     this.on('success', function(file) {
                          config.success(file);
                     });

                     this.on('error', function(file, response) {                          
                         config.error(file, response);
                     });

                     this.on('uploadprogress', function(file, progress, bytesSent) {
                            config.uploadProgress(file, progress, bytesSent);
                     });
                }

            });
        }
    };

    return {}
});