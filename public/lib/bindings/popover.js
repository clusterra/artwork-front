define(['knockout', 'jquery'], function(ko, $) {

    ko.bindingHandlers.popover = {
        init: function (element, valueAccessor) {
            var options = ko.utils.unwrapObservable(valueAccessor());

            var defaultOptions = { html: true, placement: 'bottom' };
            options = $.extend(true, {}, defaultOptions, options);

            options.content = $(options.templateSelector).html();

            $(element).popover(options);
        }
    };
    
    return {}
});