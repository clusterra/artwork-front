define(['knockout', 'jquery', '3rds/pathloader', '3rds/classie'], function(ko, $) {

    function init(elContainer) {

        var support = { animations : Modernizr.cssanimations },
        	header = elContainer.querySelector( 'header.ip-header' ),
        	loader = new PathLoader( document.getElementById( 'ip-loader-circle' ) ),
        	animEndEventNames = { 'WebkitAnimation' : 'webkitAnimationEnd', 'OAnimation' : 'oAnimationEnd', 'msAnimation' : 'MSAnimationEnd', 'animation' : 'animationend' },
        	// animation end event name
        	animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ];


        function startLoading() {
            // simulate loading something..
            var simulationFn = function(instance) {
            	var progress = 0,
            	interval = setInterval( function() {
            		progress = Math.min( progress + Math.random() * 0.1, 1 );

            		instance.setProgress( progress );

            		// reached the end
                	if( progress === 1 ) {
            			classie.remove( elContainer, 'loading' );
            			classie.add( elContainer, 'loaded' );
            			clearInterval(interval);

            			var onEndHeaderAnimation = function(ev) {
            				if( support.animations ) {
            					if( ev.target !== header ) return;
            					this.removeEventListener( animEndEventName, onEndHeaderAnimation );
            				}

            				window.removeEventListener( 'scroll', noscroll );
							$('header.ip-header').remove();
							classie.remove( elContainer, 'loaded' );
            			};

            			if( support.animations ) {
            				header.addEventListener(animEndEventName, onEndHeaderAnimation);
            			} else {
            				onEndHeaderAnimation();
            			}
            		}
            	}, 50 );
            };

            loader.setProgressFn(simulationFn);
        }


        var onEndInitialAnimation = function() {
        	if( support.animations ) {
        		this.removeEventListener(animEndEventName, onEndInitialAnimation);
        	}

        	startLoading();
        };


        function noscroll() {
            window.scrollTo( 0, 0 );
        }

        // disable scrolling
        window.addEventListener( 'scroll', noscroll);

        // initial animation
        classie.add(elContainer, 'loading');

        if (support.animations ) {
        	elContainer.addEventListener( animEndEventName, onEndInitialAnimation);
        } else {
        	onEndInitialAnimation();
        }
    }




    ko.bindingHandlers.animoprogress = {
        init: function (element, valueAccessor) {
            var options = ko.utils.unwrapObservable(valueAccessor());

           // var defaultOptions = { html: true, placement: 'bottom' };
           // options = $.extend(true, {}, defaultOptions, options);

			if (!options.firedOnce) {
				init(element);
			} else {
				$('header.ip-header').remove();
			}
        }
    };

    return {}
});