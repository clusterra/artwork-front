define(['knockout', 'jquery', '3rds/infinitescroll'], function(ko, $) {


    ko.bindingHandlers.infiscroll = {
        init: function (element, valueAccessor) {
            var options = ko.utils.unwrapObservable(valueAccessor());
            
            $(element).infinitescroll({
                loading: {
                    finishedMsg: "<p class='text-center'>" + options.finishedMsg + "</p>",
                    msgText: "<p class='text-center'>" + options.msgText + "</p>",
                    speed: 'medium',
                    img: options.img 
                },
                navSelector  	: options.navSelector,
                nextSelector 	: options.nextSelector,
                itemSelector 	: options.itemSelector,
                debug		 	: true,
                dataType	 	: 'json',
                appendCallback	: false,
                pathParse       : function(path) {
                    return path;
                },
                path            : function() {
                    return $(options.nextSelector).attr('href');
                }
            }, options.onLoadAdditionalWorksFn);

            options.resetTriggerFn(function() {
                $(element).infinitescroll('reset');
            });

            options.destroyTriggerFn(function() {
                $(element).infinitescroll('destroy');
            });
        }
    };

    return {}
});