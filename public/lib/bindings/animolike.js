define(['knockout', 'jquery', '3rds/animocons'], function(ko, $, Animocon) {


    function getExistingInstance(elButton) {
        return $(elButton).data('ani');
    }



    function initLike(elButton, elSpan, isLiked, data, likedCb, unlikedCb) {
        var ani = new Animocon(elButton, {
            tweens: [
                // ring animation
                new mojs.Transit({
                    parent: elButton,
                    duration: 750,
                    type: 'circle',
                    radius: {0: 28},
                    fill: 'transparent',
                    stroke: '#d8536f',
                    strokeWidth: {22: 0},
                    opacity: 0.2,
                    x: '45%',
                    y: '45%',
                    isRunLess: true,
                    easing: mojs.easing.bezier(0, 1, 0.5, 1)
                }),
                new mojs.Transit({
                    parent: elButton,
                    duration: 500,
                    delay: 60,
                    type: 'circle',
                    radius: {0: 20},
                    fill: 'transparent',
                    stroke: '#d8536f',
                    strokeWidth: {5: 0},
                    opacity: 0.2,
                    x: '50%',
                    y: '50%',
                    shiftX: 10,
                    shiftY: -10,
                    isRunLess: true,
                    easing: mojs.easing.sin.out
                }),

                new mojs.Transit({
                    parent: elButton,
                    duration: 800,
                    delay: 120,
                    type: 'circle',
                    radius: {0: 20},
                    fill: 'transparent',
                    stroke: '#d8536f',
                    strokeWidth: {5: 0},
                    opacity: 0.3,
                    x: '50%',
                    y: '50%',
                    shiftX: -10,
                    shiftY: 10,
                    isRunLess: true,
                    easing: mojs.easing.sin.out
                }),
                // icon scale animation
                new mojs.Tween({
                    duration: 1200,
                    easing: mojs.easing.ease.out,
                    onUpdate: function (progress) {
                        if (progress > 0.3) {
                            var elasticOutProgress = mojs.easing.elastic.out(1.43 * progress - 0.43);
                            elSpan.style.WebkitTransform = elSpan.style.transform = 'scale3d(' + elasticOutProgress + ',' + elasticOutProgress + ',1)';
                        }
                        else {
                            elSpan.style.WebkitTransform = elSpan.style.transform = 'scale3d(0,0,1)';
                        }
                    }
                })
            ],
            onCheck: function () {
                elButton.style.color = '#d8536f';
                elSpan.innerHTML = Number(elSpan.innerHTML) + 1;
                elSpan.style.color = '#d8536f';
                likedCb(data);
            },
            onUnCheck: function () {
                elButton.style.color = '#AEBECA';
                var current = Number(elSpan.innerHTML);
                elSpan.innerHTML = /*current > 1 ? */Number(elSpan.innerHTML) - 1 /*: ''*/;
                elSpan.style.color = '#aebeca';
                unlikedCb(data);
            }
        });

        ani.checked = isLiked;
        $(elButton).data('ani', ani);
    }   
    
    
    
    ko.bindingHandlers.animolike = {
        update: function (element, valueAccessor) {
            var options = ko.utils.unwrapObservable(valueAccessor());

            if (options.enabled) {
                var existingAni = getExistingInstance(element);

                if (existingAni) {
                    existingAni.checked = options.isLiked;
                } else {
                     var $button = $(element);
                     initLike($button.get(0), $button.find('span').get(0), options.isLiked, options.data, options.like, options.unlike);
                }
            }
        }
    };

    return {}
});

