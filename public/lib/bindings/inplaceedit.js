define(['knockout', 'jquery', '3rds/xeditable'], function(ko, $) {


    ko.bindingHandlers.inplaceedit = {
        init: function (element, valueAccessor) {
            var options = ko.utils.unwrapObservable(valueAccessor());



            options.activateFn(function() {
                $(element).editable({
                    rows: options.rows,
                    mode: options.mode,
                    type: options.type,

                    success: function(response, newValue) {
                        options.applyFn(newValue);
                    },

                    validate: function(value) {
                        return options.validateFn(value);
                    }
                }).on('hidden', function(e, reason) {
                      options.editCancelledFn();
                });

                $(element).editable('show');
            });
        }
    };

    return {}
});