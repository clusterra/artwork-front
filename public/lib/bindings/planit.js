define(['knockout', 'jquery', '3rds/planit.min'], function(ko, $) {

    var emptyInfoboxHtml =
        '<span class="infobox_close" aria-hidden="true">×</span>' +
        '<div class="leave_comment">' +
                '<textarea class="transparent_field" placeholder="Напишите комментарий">' +
                '</textarea>' +
                '<button class="button">Добавить комментарий</button>' +
        '</div>';



    ko.bindingHandlers.planit = {
        init: function (element, valueAccessor) {
            var options = ko.utils.unwrapObservable(valueAccessor());

            var p;

            setTimeout(function () {
                p = planit.new({
                    container: options.id,
                    image: {
                        url: options.img
                    },
                    /*markers: [
                     {
                     coords: ['50', '50'],
                     draggable: false,
                     infobox: {
                     html: "<div class='tool_comment'><div class='tool_text'>Большое спасибо за комментарий. Конечно, я не совсем согласен, но есть довольно полезные советы и мысли.</div><div class='tool_author you'>Я</div></div><hr><div class='leave_comment'><form><textarea class='transparent_field' placeholder='Напишите комментарий'></textarea><button type='submit' class='button'>Добавить комментарий</button></form></div>",
                     position: 'right',
                     arrow: true
                     },
                     class: "mine"
                     },
                     ],*/
                    markerMouseOver: function(event, marker){
                        marker.showInfobox();
                        marker.unhideInfobox();
                    }
                });

                $('.planit-markers-container').on('click', function(e){
                    if (e.target != this) {
                        return false;
                    }

                    var offset = $(this).offset();
                    var X = (e.pageX - offset.left - 14) / $(this).width() * 100;
                    var Y = (e.pageY - offset.top - 14) / $(this).height() * 100;

                    $('.planit-infobox').removeClass('active');
                    $('.planit-marker').removeClass('new_marker');

                    addMarker(X, Y);

                    var $newMarker = $('.new_marker');
                    $newMarker.mouseover();

                    var $textarea = $('#' + $newMarker.data('infobox') + ' textarea');
                    $textarea.get(0).focus();

                    $('#' + $newMarker.data('infobox') + ' button').click(function(e) {
                        var text = $textarea.val();

                        if (text.length > 0 && text.length < 500) {
                            options.click(X, Y, text);
                        }
                    });

                    $('.infobox_close').on('click', function(){
                        $('.planit-infobox').removeClass('active');
                    });
                });
            }, 200);


            function addMarker(X, Y) {
                p.addMarker({
                    coords: [X, Y],
                    draggable: false,
                    infobox: {
                        html:  emptyInfoboxHtml,
                        position: 'right',
                        arrow: true
                    },
                    class: "new_marker"
                });
            }





          /*  $('.planit-markers-container').on('click', function(e, marker){
                if(e.target != this) return false;
                var offset = $(this).offset();
                var coordX = (e.pageX - offset.left - 14) / $(this).width() * 100;
                var coordY = (e.pageY - offset.top - 14) / $(this).height() * 100;
                $('.planit-infobox').removeClass('active');
                $('.planit-marker').removeClass('new_marker');
                p.addMarker({
                    coords: [coordX, coordY],
                    draggable: false,
                    infobox: {
                        html: "<span class='infobox_close' aria-hidden='true'>×</span><div class='clearfix'></div><div class='tool_comment'><div class='tool_text'>Нужно полностью поменять все шрифты. Один шрифт на весь лендинг это очень плохо. Выбранный шрифт с натяжкой подойдет только для заголовков. Основной текст слишком крупный. Плюс постоянные картинки на фонах тоже все очень ухудшают.</div><span class='pre_author'>&mdash;</span><div class='tool_author'>Артём Абзанов</div><span class='tip_date pull-right'>24 МАЯ В 23:15</span></div><hr><div class='tool_comment'><div class='tool_text'>Большое спасибо за комментарий. Конечно, я не совсем согласен, но есть довольно полезные советы и мысли.</div><span class='pre_author'>&mdash;</span><div class='tool_author you'>Я</div><span class='tip_date pull-right'>24 МАЯ В 23:15</span></div><hr><div class='leave_comment'><form><textarea class='transparent_field' placeholder='Напишите комментарий'></textarea><button type='submit' class='button'>Добавить комментарий</button></form></div>",
                        position: 'right',
                        arrow: true
                    },
                    class: "new_marker"
                });
                $('.new_marker').mouseover();
                $('.infobox_close').on('click', function(){
                    $('.planit-infobox').removeClass('active');
                });
            });*/
        }
    };

    return {}
});