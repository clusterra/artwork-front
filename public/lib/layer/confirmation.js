define([], function () {

    // here place some static init code if required as in Java after class loading


    // may be used both with 'new' and without
    return function () {

        function init($bodySlot, contentConf, doneFn) {
            $bodySlot.html([
                contentConf.text,
                '<hr>',
                '<div class="centered_btns"><a class="btn btn_primary">', contentConf.buttonConfirm ,'</a><a class="btn ghost_button">Отмена</a></div>',
                '<div class="clearfix"></div>'
                ].join('')
            );

            $bodySlot.find('a.btn_primary').click(function() {
                doneFn();
                contentConf.confirmCb();
            });

            $bodySlot.find('a.ghost_button').click(function() {
                doneFn();
            });
        }


        return {
            deploy: function(config) {
                init(config.slots.body, config.contentConf, config.doneFn);

                return this;
            }
        }
    };
});